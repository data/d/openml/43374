# OpenML dataset: Kaggle-YouTube-Video-Metadata

https://www.openml.org/d/43374

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Introduction

The data id collected using YouTube Data Tools from Kaggle YouTube channel. It shows information about all videos from this channel, starting with 2018.
Data collection
Using YouTube Data Tools one can access the metadata for YouTube channels, videos, comments, upvotes. 
The data was collected from the Kaggle YouTube video channel.  
It covers a time period starting from 2018 until today.
References

YouTube Data Tools, https://tools.digitalmethods.net/  

Inspiration
Use this amazing dataset to analyze the impact of these videos, by looking to view, like, dislike, favorite, comments. Try to understand from description of the video if some subjects have larger impact. Factor-in the age of each video, with this amazing dataset collecting video metadata starting from 2007.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43374) of an [OpenML dataset](https://www.openml.org/d/43374). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43374/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43374/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43374/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

